const { writeFileSync } = require('fs')

const branchMap = new Map([
  ['main', 'production'],
  ['staging', 'acceptance'],
  ['develop', 'develop']
])

const createJob = environment => `
deploy:${environment}:
  stage: deploy
  image: docker
  environment: 
    name: ${environment}
  variables:
    ENVIRONMENT: ${environment}
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - echo 'deploy to ${environment} 🚀🚀!'
    - docker build --build-arg ENVIRONMENT=${environment} --pull --cache-from $CI_REGISTRY_IMAGE --tag $CI_REGISTRY_IMAGE/${environment} .
    - docker push $CI_REGISTRY_IMAGE/${environment}
`

const createDynamicGitLabFile = () => {
  /**
   * CI_COMMIT_REF_NAME is a built-in runtime environment in GitLab. Read more
   * in the docs at: https://docs.gitlab.com/ee/ci/variables/#list-all-environment-variables
   */
  const { CI_COMMIT_REF_NAME } = process.env

  // Map the branch name to the equivalent environment
  const environment = branchMap.get(CI_COMMIT_REF_NAME)

  // create content of ci file
  const jobContent = createJob(environment)

  // write file to disc
  writeFileSync('dynamic-gitlab-ci.yml', jobContent)
}

createDynamicGitLabFile()